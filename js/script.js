function validate (element){
    if ($(element).hasClass('warning')){
        $(element).removeClass('warning');
        $('.warning-text').remove();
    }

    if (isNaN(+ element.val())|| +element.val() < 1) {
        element.addClass('warning');
        const msgElem = $('<p>').attr({'class': 'warning-text'}).text(`It isn't a number`);
        $(msgElem).insertBefore('#circle-draw');
    }
}


$(document).ready(function () {
    $(document).click(function (e) {
        if ($(e.target)[0].id === 'draw') {
            $('button').remove('#draw');
            const circleDiameter = $('<input>').attr({'id':'diameter', 'placeholder':'Enter the circle diameter in pixels'});
            const circleDraw = $('<button>').attr({'id':'circle-draw'}).text('Draw');
            $(document.body).append(circleDiameter);
            $(document.body).append(circleDraw);
        }

        if ($(e.target)[0].id === 'circle-draw') {
            validate($('#diameter'));
            if (!isNaN(+ $('#diameter').val())|| +$('#diameter').val() > 0) {
                const diameterValue = +$('#diameter').val();
                const circles = $('<div>').attr({'id': 'circles'});
                $(document.body).append(circles);
                circles.width(`${diameterValue * 10}px`);

                for (let i = 1; i <= 100; i++) {
                    const circle = $('<div>').attr({'class': 'circle'});
                    let x = Math.floor(Math.random() * 256);
                    let y = Math.floor(Math.random() * 256);
                    let z = Math.floor(Math.random() * 256);
                    circle.css({
                        'background-color': `rgb(${x},${y},${z})`,
                        width: `${diameterValue}px`,
                        height: `${diameterValue}px`
                    });
                    $('#circles').append(circle);
                }
            }
        }
        let target = $(e.target)[0];
        while (target !== document) {
            if($(target).hasClass('circle')) {
                $(target).hide();
                return;
            }
            target = $(target)[0].parent();
        }
    })
});